#include <iostream>
#include <boost/asio.hpp>
#include "zxserver.h"
#include "zxcmd.h"
using namespace std;
namespace asio = boost::asio;

int main()
{
    try {
        asio::io_service service;
        zx::server server(service);
        server.run();
    } catch (std::exception& e) {
        std::cerr << e.what() << std::endl;
    }

    return 0;
}

