/// ===========================================================================
/// zxserver.cpp
/// Servidor gráfico simples
///
/// Marcos Medeiros, 2014
/// ---------------------------------------------------------------------------
/// TODO:
///     Implementar um buffer com contagem  de  referência  para as  leituras e
/// escritas assíncronas,  o buffer  deve sobreviver até que a próprio asio não
/// o utilize mais. No momento o buffer sobrevive pelo mesmo  tempo que session
/// no entanto, ao chamar o método close_me no servidor o shared_ptr de session
/// chega a zero e ele é destruido,  muito provavelmente enquanto  a asio ainda
/// está utilizando o buffer.
/// ===========================================================================
#include <iostream>
#include <exception>
#include <functional>
#include <exception>
#include <boost/asio.hpp>
#include <allegro5/allegro.h>
#include <thread>
#include "zxserver.h"
#include "zxcmd.h"
#include "zxbuffers.h"


namespace asio = boost::asio;
using boost::system::error_code;
using asio::ip::tcp;

namespace zx
{

///////////////////////////////////////////////////////////////////////////////
///                                                                         ///
///                            SERVIDOR                                     ///
///                                                                         ///
///////////////////////////////////////////////////////////////////////////////

server::server(asio::io_service &service) : m_service(service), m_socket(service),
    m_max_queue_events(50)
{
    m_clients_ids = 1;
    try {
        m_acceptor = new tcp::acceptor(m_service, tcp::endpoint(tcp::v4(), 7070));
        do_accept();
    } catch (std::exception& e) {
        std::cerr << "Erro ao iniciar o servidor!" << std::endl;
        std::cerr << e.what() << std::endl;
    }
}


server::~server()
{
    m_service.stop();
}


// ============================================================================
// Pede ao servidor que remova todas as referência a sessão que ele tem, uma
// vez que ele é um shared_ptr quando zeramos a contagem de referências ele
// será destruido.
// ============================================================================
void server::close_me(session::ptr ses)
{
    std::cout << "Refs: " << ses.use_count() << std::endl;
    m_top_windows.remove_if([&](window::ptr const& p) { return p->owner == ses; });
    m_sessions.remove_if([&](session::ptr const& p) { return p == ses; });
    std::cout << "Fechando sessão: " << ses->id() << std::endl;
}


// ============================================================================
// Roda o servidor, colocando a io_service na sua própria thread e chama o
// loop principal para que os eventos possam ser processados e despachados
// ============================================================================
void server::run()
{
    setup_display();
    std::thread service_thread([&] { m_service.run(); });
    service_thread.detach();

    event_loop();
}


// ============================================================================
// Aceita conexões assincronamente
// ============================================================================
void server::do_accept()
{
    m_acceptor->async_accept(m_socket,
                             [this](error_code)
    {
        int id = m_clients_ids++;
        std::cout << "O cliente conectou:" << id << std::endl;
        auto ses = std::make_shared<session>(ptr(this),
                                            std::move(m_socket), id);
        m_sessions.push_back(ses);
        ses->start();
        do_accept();
    });
}


// ============================================================================
// Loop principal
// ============================================================================
void server::event_loop()
{
    try {
        while (true) {
            if (!low_event_loop()) {
                std::exit(0);
            }
            high_event_loop();
            update_display();
        }
    } catch (std::exception& e) {
        std::cerr << e.what() << std::endl;
    }
}

bool server::enqueue_event(cmd::response::ptr ev)
{
    while (m_events.size() > m_max_queue_events)
        m_events.pop_front();
    m_events.push_back(ev);
    return true;
}

void server::push_draw_target()
{
    m_target_stack.push(al_get_target_bitmap());
}

void server::pop_draw_target()
{
    if (!m_target_stack.empty()) {
        auto target = m_target_stack.top();
        m_target_stack.pop();
        al_set_target_bitmap(target);
    }
}


///////////////////////////////////////////////////////////////////////////////
///                                                                         ///
///                             SESSÃO                                      ///
///                                                                         ///
///////////////////////////////////////////////////////////////////////////////

session::session(server::ptr srv, tcp::socket socket, int id) :
    m_server(srv), m_socket(std::move(socket)), m_id(id)
{
}

int session::id()
{
    return m_id;
}

void session::start()
{
    do_read_header();
}

tcp::socket& session::socket()
{
    return m_socket;
}

// ============================================================================
// Envia uma resposta para o cliente
// ============================================================================
void session::send_response(cmd::response& response)
{
    //std::cout << "session::send_response()" << std::endl;
    assert(response.size < cmd_packet::max_body_length);

    cmd_packet packet;
    packet.body_length(response.size);
    packet.encode_header();
    std::memcpy(packet.body(), &response, response.size);

    int this_packet = response.packet_number;

    auto writer = [&, this_packet](error_code, std::size_t) {};

    auto buffer = shared_const_buffer(packet.data(), packet.length());
    asio::async_write(m_socket, buffer, writer);
}


// ============================================================================
// Lê o cabeçalho de um comando, no momento ele é composto
// apenas do tamanho do corpo do comando.
// ============================================================================
void session::do_read_header()
{
    auto self(shared_from_this());
    auto buffer = asio::buffer(m_read_cmd.data(), cmd_packet::header_length);

    auto reader = [this, self](error_code ec, std::size_t) {
        if (!ec && m_read_cmd.decode_header()) {
            do_read_body();
        }
        if (ec == asio::error::eof) {
            std::cout << "O cliente " << m_id << " desconectou!" << std::endl;
            // cancela todas as operações no socket
            //m_socket.close();
            m_server->close_me(shared_from_this());
        }
    };

    // faz a leitura assíncrona do cabeçalho
    asio::async_read(m_socket, buffer, reader);
}


// ============================================================================
// Lê o corpo de um comando
// ============================================================================
void session::do_read_body()
{
    auto self(shared_from_this());

    auto buffer = asio::buffer(m_read_cmd.body(), m_read_cmd.body_length());

    auto reader = [this, self](error_code ec, std::size_t) {
        if (!ec) {
            // decodifica e despacha o comando no servidor
            m_server->decode_and_dispatch(self, m_read_cmd);
            do_read_header();
        }
    };

    // faz a leitura assíncrona do corpo do comando
    asio::async_read(m_socket, buffer, reader);
}


}
