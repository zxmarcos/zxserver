/// ===========================================================================
/// zxdisplay.cpp
/// Servidor gráfico simples
///
/// Marcos Medeiros, 2014
/// ===========================================================================
#include <iostream>
#include <exception>
#include <functional>
#include <exception>
#include <boost/range/adaptor/reversed.hpp>
#include <allegro5/allegro.h>
#include <allegro5/allegro_ttf.h>
#include <allegro5/allegro_color.h>
#include <allegro5/allegro_primitives.h>
#include "zxserver.h"


namespace zx {

// ============================================================================
// Cria e configura a janela que representa nosso desktop
// ============================================================================
void server::setup_display()
{
    if (!al_init())
        throw new std::runtime_error("Falha ao iniciar o Allegro5!");

    al_init_font_addon();
    al_init_ttf_addon();
    al_init_primitives_addon();

    al_set_new_display_option(ALLEGRO_VSYNC, 2, ALLEGRO_REQUIRE);

    m_display = al_create_display(800, 600);
    if (m_display == nullptr)
        throw new std::runtime_error("Falha ao iniciar o desktop!");

    m_display_events = al_create_event_queue();
    if (m_display_events == nullptr)
        throw new std::runtime_error("Falha ao iniciar lista de eventos!");

    al_install_keyboard();
    al_install_mouse();

    al_register_event_source(m_display_events, al_get_display_event_source(m_display));
    al_register_event_source(m_display_events, al_get_keyboard_event_source());
    al_register_event_source(m_display_events, al_get_mouse_event_source());

    m_display_font = al_load_font("Envy Code R.ttf", 14, 0);
    if (!m_display_font)
        std::cout << "Falha ao carregar fonte!" << std::endl;

    al_set_target_bitmap(al_get_backbuffer(m_display));
    al_clear_to_color(al_map_rgb(255, 255, 255));
    al_flip_display();

    m_mouse.dragging = false;
}


// ============================================================================
// Processa todos os eventos baixos recebidos do allegro5
// ============================================================================
bool server::low_event_loop()
{
    ALLEGRO_EVENT event;

    while (al_get_next_event(m_display_events, &event)) {

        cmd::response::ptr r = std::make_shared<cmd::response>();

        switch (event.type) {
        case ALLEGRO_EVENT_DISPLAY_CLOSE:
            r->type = cmd::ci_server_shutdown;
            r->error_code = 0;
            enqueue_event(r);
            // despacha todos os eventos e sai!
            high_event_loop();
            al_flush_event_queue(m_display_events);
            return false;

        // Eventos do mouse
        case ALLEGRO_EVENT_KEY_DOWN:
            r->type = cmd::ci_keyboard_press_event;
            goto _keyboard_event;
        case ALLEGRO_EVENT_KEY_UP:
            r->type = cmd::ci_keyboard_release_event;
        _keyboard_event:
            r->error_code = 0;
            r->keyboard.keycode = event.keyboard.keycode;
            r->keyboard.unichar = event.keyboard.unichar;
            r->keyboard.modifier = event.keyboard.modifiers;
            if (m_focused)
                m_focused->enqueue_event(r);
            break;

        // --------------------------------------------------------------------
        // Eventos do mouse
        // --------------------------------------------------------------------
        case ALLEGRO_EVENT_MOUSE_BUTTON_DOWN: {
            m_mouse.begin(event);
            r->type = cmd::ci_mouse_press_event;
            window::ptr p = window_at(event.mouse.x, event.mouse.y);
            // se clicou sobre uma janela, trazer para frente
            if (p) {
                rise_parent_window(p);
                cmd::response::ptr f = std::make_shared<cmd::response>();
                f->focus.handle = p->handle;
                f->type = cmd::ci_grab_focus;
                p->enqueue_event(f);

                // a janela recebe eventos de botões pressionados
                if (p->event_flags & cmd::MousePress) {
                    r->mouse.x = event.mouse.x;
                    r->mouse.y = event.mouse.y;
                    r->mouse.z = event.mouse.z;
                    r->mouse.button = event.mouse.button;
                    p->enqueue_event(r);
                }

                if (is_in_frame(p, m_mouse.x, m_mouse.y)) {
                    m_mouse.drag_window = p;
                    m_mouse.dragging = true;
                }
            }
            m_mouse.end();
            break;
        }
        case ALLEGRO_EVENT_MOUSE_BUTTON_UP:
            m_mouse.begin(event);
            r->type = cmd::ci_mouse_release_event;
            m_mouse.drag_window = window::ptr();
            m_mouse.dragging = false;
            m_mouse.end();
            break;

        case ALLEGRO_EVENT_MOUSE_AXES:
            m_mouse.begin(event);
            if (m_mouse.dragging && m_mouse.drag_window) {
                m_mouse.drag_window->rect.x += m_mouse.dx();
                m_mouse.drag_window->rect.y += m_mouse.dy();
            }
            m_mouse.end();
            break;
        }
        }
    return true;
}


// ============================================================================
// Atualiza a tela
// ============================================================================
void server::update_display()
{
    al_set_target_backbuffer(m_display);
    //al_set_target_bitmap(al_get_backbuffer(m_display));
    al_clear_to_color(al_map_rgb(0x33, 0x66, 0xCC));

    al_draw_text(m_display_font, al_map_rgb(255,255,255), 10, 10, 0,
                 "ZxServer: powered by C++11, boost::asio, Allegro5");

    std::function<void(window::ptr const&)> recursive_draw = [&](window::ptr const& w) {
        if (w) {
            al_draw_bitmap(w->surface, w->rect.x, w->rect.y, 0);
            push_draw_target();
            al_set_target_bitmap(w->surface);
            for (window::ptr const& child : w->children) {
                recursive_draw(child);
            }
            pop_draw_target();
        }
    };

    int zindex = 1;
    for (window::ptr win : boost::adaptors::reverse(m_top_windows)) {
        render_shadow(win);
        render_frame(win, zindex);
        zindex++;
        recursive_draw(win);
    }

    al_flip_display();
    al_rest(0.001);
}


// ============================================================================
// Despacha todos os eventos na fila
// ============================================================================
void window::flush()
{
    while (!event_queue.empty()) {
        auto p = event_queue.front();
        event_queue.pop_front();
        owner->send_response(*p);
    }
}


// ============================================================================
// Enfilera um evento
// ============================================================================
void window::enqueue_event(cmd::response::ptr ev)
{
    event_queue.push_back(ev);
}

window::window() : surface(nullptr)
{
}

window::~window()
{
    if (surface != nullptr)
        al_destroy_bitmap(surface);
}


// ============================================================================
// Desenha a moldura envolta da janela
// ============================================================================
void server::render_frame(window::ptr win, int zindex)
{
    int wins = m_top_windows.size();
    if (zindex <= 0)
        zindex = 1;
    xrect r = frame_rect(win);
    float factor = static_cast<float>(zindex) / static_cast<float>(wins);
    ALLEGRO_COLOR col = al_map_rgb_f(factor, 0, 0);

    al_draw_filled_rectangle(r.x,
                             r.y,
                             r.x + r.w,
                             r.y + r.h,
                             col);

    al_draw_text(m_display_font, al_map_rgb(255, 255, 255),
                 r.x, r.y, 0, win->title.data());
}


// ============================================================================
// Desenha a sombra sob a janela
// ============================================================================
void server::render_shadow(window::ptr win)
{
    xrect r = win->rect.adjusted(5, 5, 0, 0);
    al_draw_filled_rectangle(r.x,
                             r.y,
                             r.x + r.w,
                             r.y + r.h,
                             al_map_rgba(0, 0, 0, 100));
}


// ============================================================================
// Coloca a janela no topo e em foco
// ============================================================================
void server::rise_window(window::ptr win)
{
    m_top_windows.remove(win);
    m_top_windows.push_front(win);
    m_focused = win;
}

void server::rise_parent_window(const window::ptr& win)
{
    rise_window(top_parent(win));
}

window::ptr server::top_parent(const window::ptr& child)
{
    window::ptr p = child;
    window::ptr last;
    while (p) {
        last = p;
        p = p->parent;
    }
    return last;
}


// ============================================================================
// Retorna o retângulo da moldura da janela
// ============================================================================
xrect server::frame_rect(const window::ptr& win)
{
    return win->rect.adjusted(-1, -m_display_font->height,
                              +2,  m_display_font->height + 1);
}


// ============================================================================
// Retorna qual janela está na posição (x, y)
// ============================================================================
window::ptr server::window_at(int x, int y)
{
    for (window::ptr w : m_top_windows) {
        if (frame_rect(w).contains(x, y)) {
            window::ptr p = child_at(w, x, y);
            if (p) {
                return p;
            }
            return w;
        }
    }
    return window::ptr();
}


// ============================================================================
// Retorna qual janela filha está na posição (x, y)
// ============================================================================
window::ptr server::child_at(const window::ptr& win, int x, int y)
{
    if (!win || win->children.empty())
        return window::ptr();

    for (const window::ptr& child : win->children) {
        xpoint p = child->map_to_display();
        xrect cr(p, child->size());
        if (cr.contains(x, y)) {
            auto c = child_at(child, x, y);
            if (c)
                return c;
            return child;
        }
    }
    return window::ptr();
}


// ============================================================================
// Mapeia a janela para a tela
// ============================================================================
xpoint window::map_to_display()
{
    xpoint pos = rect.top_left();
    window::ptr p = parent;
    while (p) {
        pos += p->rect.top_left();
        p = p->parent;
    }
    return pos;
}

xsize window::size() const
{
    return rect.size();
}


// ============================================================================
// Verifica se uma coordenada está dentro da moldura (excluindo as bordas)
// ============================================================================
bool server::is_in_frame(const window::ptr& win, int x, int y)
{
    xrect fr = frame_rect(win);
    fr.h = m_display_font->height;
    return fr.contains(x, y);
}

}
