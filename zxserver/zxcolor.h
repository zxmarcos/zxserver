#pragma once

namespace zx
{

struct xcolor {
    char r;
    char g;
    char b;
    char a;
    xcolor(char r=0, char g=0, char b=0, char a=255) :
        r(r), g(g), b(b), a(a) {
    }
};

}
