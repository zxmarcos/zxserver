#pragma once

namespace zx
{

struct xsize {
    int w;
    int h;
    xsize(int w=0, int h=0) : w(w), h(h) {
    }
    xsize(const xsize& rhs) {
        w = rhs.w;
        h = rhs.h;
    }
};

struct xpoint {
    int x;
    int y;
    xpoint(int x=0, int y=0) : x(x), y(y) {
    }

    xpoint(const xpoint& rhs) {
        x = rhs.x;
        y = rhs.y;
    }

    xpoint& operator+=(const xpoint& rhs) {
        x += rhs.x;
        y += rhs.y;
        return *this;
    }

    xpoint& operator-=(const xpoint& rhs) {
        x -= rhs.x;
        y -= rhs.y;
        return *this;
    }

    xpoint operator+(const xpoint& rhs) {
        return xpoint(x + rhs.x, y + rhs.y);
    }

    xpoint operator-(const xpoint& rhs) {
        return xpoint(x - rhs.x, y - rhs.y);
    }
};

struct xrect {
    int x;
    int y;
    int w;
    int h;

    xrect(int x=0, int y=0, int w=0, int h=0) :
        x(x), y(y), w(w), h(h) {
    }

    xrect(const xpoint& p, const xsize& s) {
        x = p.x;
        y = p.y;
        w = s.w;
        h = s.h;
    }

    xrect adjusted(int x2, int y2, int w2, int h2) {
        return xrect(x + x2, y + y2, w + w2, h + h2);
    }

    bool contains(int px, int py) {
        if (px >= x && px <= (x + w))
            if (py >= y && py <= (y + h))
                return true;
        return false;
    }

    bool contains(const xpoint& p) {
        return contains(p.x, p.y);
    }

    xpoint top_left() const {
        return xpoint(x, y);
    }

    xsize size() const {
        return xsize(w, h);
    }

};





}
