/// ===========================================================================
/// zxserver.h
/// Servidor gráfico simples
///
/// Marcos Medeiros, 2014
/// ===========================================================================
#pragma once

#include <iostream>
#include <boost/asio.hpp>
#include <boost/lockfree/queue.hpp>
#include <memory>
#include <list>
#include <string>
#include <cstring>
#include <atomic>
#include <deque>
#include <set>
#include <allegro5/allegro.h>
#include <allegro5/allegro_font.h>
#include <stack>
#include "zxpacket.h"
#include "zxcmd.h"
#include "zxgeom.h"

namespace zx
{
namespace asio = boost::asio;
using asio::ip::tcp;
using boost::system::error_code;

class server;
class session;
class window;

// ============================================================================
// Classe representando uma sessão com um cliente
// ============================================================================
class session : public std::enable_shared_from_this<session> {
    std::shared_ptr<server> m_server;
    tcp::socket m_socket;
    int m_id;
    cmd_packet m_read_cmd;
    cmd_packet m_write_response;

    void do_read_header();
    void do_read_body();
public:
    typedef std::shared_ptr<session> ptr;
    session(std::shared_ptr<server> srv, tcp::socket socket, int id);
    int id();
    void start();
    tcp::socket& socket();
    void send_response(cmd::response& response);
};


// ============================================================================
// Representação da janela e seus atributos
// ============================================================================
struct window : public std::enable_shared_from_this<window> {
    typedef std::shared_ptr<window> ptr;
    session::ptr owner;
    std::string title;
    window::ptr parent;
    int handle;
    unsigned event_flags;
    xrect rect;
    ALLEGRO_BITMAP* surface;
    std::deque<cmd::response::ptr> event_queue;
    std::list<window::ptr> children;

    xpoint map_to_display();
    xsize size() const;
    void flush();
    void enqueue_event(cmd::response::ptr ev);
    explicit window();
    ~window();
};


// ============================================================================
// Classes úteis para representar estados internos
// ============================================================================
namespace internal {
struct mouse_state {
    int x = 0;
    int y = 0;
    int z;
    int button;
    int prev_x = 0;
    int prev_y = 0;
    int prev_z;
    int prev_button;

    int dx() {
        return x - prev_x;
    }

    int dy() {
        return y - prev_y;
    }

    int dz() {
        return prev_z - z;
    }

    void end() {
        prev_x = x;
        prev_y = y;
        prev_z = z;
        prev_button = button;
    }

    void begin(const ALLEGRO_EVENT& event) {
        x = event.mouse.x;
        y = event.mouse.y;
        z = event.mouse.z;
        button = event.mouse.button;
    }

    bool dragging;
    window::ptr drag_window;
};

}


// ============================================================================
// Nosso servidor gráfico
// ============================================================================
class server : public std::enable_shared_from_this<server> {
    asio::io_service& m_service;
    tcp::acceptor* m_acceptor;
    std::list<session::ptr> m_sessions;
    tcp::socket m_socket;

    ALLEGRO_DISPLAY* m_display;
    ALLEGRO_EVENT_QUEUE* m_display_events;
    ALLEGRO_FONT* m_display_font;

    void setup_display();
    bool low_event_loop();
    bool high_event_loop();
    std::atomic_int m_clients_ids;
    void update_display();

    std::list<window::ptr> m_top_windows;
    std::map<int, window::ptr> m_windows;
    void event_loop();

    // número máximo de eventos na fila gerados no servidor
    const int m_max_queue_events;
    std::deque<cmd::response::ptr> m_events;

    // coloca na fila um evento
    bool enqueue_event(cmd::response::ptr ev);

    window::ptr m_focused;

    internal::mouse_state m_mouse;

    std::stack<ALLEGRO_BITMAP*> m_target_stack;
    void push_draw_target();
    void pop_draw_target();

public:
    typedef std::shared_ptr<server> ptr;
    server(asio::io_service& service);
    ~server();

    void close_me(session::ptr ses);
    void run();
    void do_accept();
    void decode_and_dispatch(session::ptr owner, cmd_packet& packet);

private:
    // commandos
    void impl_create_window(session::ptr owner, cmd::create_window* pd);
    void impl_register_listener(session::ptr owner, cmd::register_listener* pd);
    void impl_window_title(session::ptr owner, cmd::window_title* pd);
    void impl_draw_point(session::ptr owner, cmd::draw_point* pd);
    void impl_draw_line(session::ptr owner, cmd::draw_line* pd);
    void impl_draw_rect(session::ptr owner, cmd::draw_rect* pd);
    void impl_draw_filed_rect(session::ptr owner, cmd::draw_filled_rect* pd);
    void impl_query_attributes(session::ptr owner, cmd::query_attributes* pd);
    void impl_set_attributes(session::ptr owner, cmd::set_attributes* pd);

    // funções úteis
    void insert_window(window::ptr win);
    void remove_window(window::ptr win);
    void render_frame(window::ptr win, int zindex=1);
    void render_shadow(window::ptr win);
    void rise_window(window::ptr win);
    void rise_parent_window(const window::ptr& win);
    window::ptr top_parent(const window::ptr& child);
    xrect frame_rect(const window::ptr& win);

    window::ptr window_at(int x, int y);
    window::ptr child_at(const window::ptr& win, int x, int y);
    bool is_in_frame(const window::ptr& win, int x, int y);
};

};
