﻿#pragma once
#include <memory>
#include <vector>
#include <boost/asio.hpp>
#include <iostream>
namespace zx
{

#define DEBUG_SHARED_CONST_BUFFER 0

namespace asio = boost::asio;
using asio::ip::tcp;
using boost::system::error_code;


class shared_const_buffer {
    std::shared_ptr<std::vector<char>> m_data;
    asio::const_buffer m_buffer;
public:
#if DEBUG_SHARED_CONST_BUFFER
    static void __ptr_dtor(std::vector<char> *ptr) {
        std::cout << "Destruindo shared_const_buffer: " << ptr << std::endl;
        delete ptr;
    }
#endif

    // Implementa a interface para ConstBufferSequence
    typedef asio::const_buffer value_type;
    typedef const asio::const_buffer* const_iterator;
    const asio::const_buffer* begin() const {
        return &m_buffer;
    }
    const asio::const_buffer* end() const {
        return &m_buffer + 1;
    }

    explicit shared_const_buffer(const void* data, std::size_t length) :
        m_data(new std::vector<char>(length)
           #if DEBUG_SHARED_CONST_BUFFER
               ,__ptr_dtor
           #endif
               ),
        m_buffer(asio::buffer(*m_data))
    {
#if DEBUG_SHARED_CONST_BUFFER
        std::cout << "Alocando shared_const_buffer: " << m_data.get()
                  << " : " << m_data->size() << std::endl;
#endif
        // faz uma cópia profunda
        std::memcpy(&m_data->at(0), data, length);
    }

};

}
