/// ===========================================================================
/// zxdispatcher.cpp
/// Servidor gráfico simples
///
/// Marcos Medeiros, 2014
/// ===========================================================================
#include <iostream>
#include <exception>
#include <functional>
#include <exception>
#include <boost/asio.hpp>
#include <allegro5/allegro.h>
#include <allegro5/allegro_primitives.h>
#include <thread>
#include <atomic>
#include "zxserver.h"
#include "zxcmd.h"

const bool DEBUG_PACKETS = true;

namespace zx
{


// ============================================================================
// Decodifica os comandos e os despacha
// ============================================================================
void server::decode_and_dispatch(session::ptr owner, cmd_packet& packet)
{
    cmd::class_* p = reinterpret_cast<cmd::class_*>(packet.body());

    if (DEBUG_PACKETS)
        std::printf("[%5d] ", p->packet_number);

    switch (p->type) {
    case cmd::ci_create_window: {
        auto d = static_cast<cmd::create_window*>(p);
        if (DEBUG_PACKETS)
            std::printf("create_window %d,%d,%d,%d\n", d->x, d->y, d->width, d->height);
        impl_create_window(owner, d);
        break;
    }

    case cmd::ci_register_listener: {
        auto d = static_cast<cmd::register_listener*>(p);
        if (DEBUG_PACKETS)
            std::printf("register_listener %d,%X\n", d->handle, d->flags);
        impl_register_listener(owner, d);
        break;
    }

    case cmd::ci_window_title: {
        auto d = static_cast<cmd::window_title*>(p);
        if (DEBUG_PACKETS)
            std::printf("set_window_title %d, %s\n", d->handle, d->name);
        impl_window_title(owner, d);
        break;
    }

    case cmd::ci_draw_point: {
        auto d = static_cast<cmd::draw_point*>(p);
        if (DEBUG_PACKETS)
            std::printf("draw_point %d, (%d, %d)\n", d->handle, d->x, d->y);
        impl_draw_point(owner, d);
        break;
    }

    case cmd::ci_draw_line: {
        auto d = static_cast<cmd::draw_line*>(p);
        if (DEBUG_PACKETS)
            std::printf("draw_line %d, (%d, %d), (%d, %d)\n", d->handle,
                        d->x1, d->y1, d->x2, d->y2);
        impl_draw_line(owner, d);
        break;
    }

    case cmd::ci_draw_rect: {
        auto d = static_cast<cmd::draw_rect*>(p);
        if (DEBUG_PACKETS)
            std::printf("draw_rect %d, [%d, %d, %d, %d]\n", d->handle,
                        d->r.x, d->r.y, d->r.w, d->r.h);
        impl_draw_rect(owner, d);
        break;
    }

    case cmd::ci_draw_filled_rect: {
        auto d = static_cast<cmd::draw_filled_rect*>(p);
        if (DEBUG_PACKETS)
            std::printf("draw_filled_rect %d, [%d, %d, %d, %d]\n", d->handle,
                        d->r.x, d->r.y, d->r.w, d->r.h);
        impl_draw_filed_rect(owner, d);
        break;
    }

    case cmd::ci_query_attributes: {
        auto d = static_cast<cmd::query_attributes*>(p);
        if (DEBUG_PACKETS)
            std::printf("query_attributes %d\n", d->handle);
        impl_query_attributes(owner, d);
        break;
    }

    case cmd::ci_set_attributes: {
        auto d = static_cast<cmd::set_attributes*>(p);
        if (DEBUG_PACKETS)
            std::printf("set_attributes %d, %x\n", d->handle, d->flags);
        impl_set_attributes(owner, d);
        break;
    }



    }

}


// ============================================================================
// Despacha os eventos que estão na lista
// ============================================================================
bool server::high_event_loop()
{
#if 0
    while (!m_events.empty()) {
        cmd::response::ptr p = m_events.front();
        m_events.pop_front();

        switch (p->type) {
        case cmd::ci_keyboard_press_event:
        case cmd::ci_keyboard_release_event:
            if (m_focused)
                m_focused->owner->send_response(*p);
            break;

        case cmd::ci_mouse_move_event:
            for (window::ptr const& w : m_listeners.mouse_move)
                if (w->rect.contains(p->mouse.x, p->mouse.y))
                    w->owner->send_response(*p);
            break;

        case cmd::ci_mouse_press_event:
            for (window::ptr const& w : m_listeners.mouse_press)
                if (w->rect.contains(p->mouse.x, p->mouse.y))
                    w->owner->send_response(*p);
            break;

        case cmd::ci_mouse_release_event:
            for (window::ptr const& w : m_listeners.mouse_release)
                if (w->rect.contains(p->mouse.x, p->mouse.y))
                    w->owner->send_response(*p);
            break;

            // todos os outros eventos são despachados em broadcast
        default:
            for (window::ptr const& w : m_windows)
                w->owner->send_response(*p);
            break;
        }

    }
#else
    for (window::ptr const& w : m_top_windows) {
        w->flush();
    }
#endif
    return true;
}


// ============================================================================
// Cria uma janela
// ============================================================================
void server::impl_create_window(session::ptr owner, cmd::create_window* pd)
{
    al_set_target_backbuffer(nullptr);

    // cria a resposta
    cmd::response r;
    r.packet_number = pd->packet_number;

    if (pd->parent && m_windows.find(pd->parent) == m_windows.end()) {
        r.error_code = 1;
        std::cerr << "Parent inválido!" << std::endl;
        owner->send_response(r);
        return;
    }

    static std::atomic_int handle_counter(10000);
    window::ptr win = std::make_shared<window>();
    win->owner = owner;
    win->handle = ++handle_counter;
    win->rect = xrect(pd->x, pd->y, pd->width, pd->height);
    win->surface = al_create_bitmap(pd->width, pd->height);

    if (pd->parent) {

        // coloca a janela como filho de outra
        window::ptr parent = m_windows[pd->parent];
        if (parent) {
            parent->children.push_back(win);
            win->parent = parent;
        }
    }

    // Falha ao criar a janela
    if (win->surface == nullptr) {
        r.error_code = 1;
        owner->send_response(r);
        return;
    }

    // Pinta a janela
    al_set_target_bitmap(win->surface);
    al_clear_to_color(al_map_rgb(240, 240, 240));
    al_set_target_bitmap(al_get_backbuffer(m_display));

    insert_window(win);

    r.error_code = 0;
    cmd::resp_create_window& rw = r.create_win;
    rw.handle = win->handle;
    owner->send_response(r);

}


// ============================================================================
// Registra a janela para escutar alguns eventos
// Os eventos de teclado só serão despachados se a janela possuir o foco.
// ============================================================================
void server::impl_register_listener(session::ptr owner, cmd::register_listener* pd)
{
    cmd::response r;
    r.packet_number = pd->packet_number;

    if (m_windows.find(pd->handle) == m_windows.end()) {
        r.error_code = 1;
        owner->send_response(r);
        return;
    }

    window::ptr win = m_windows[pd->handle];
    win->event_flags = pd->flags;

    if (pd->flags & cmd::MousePress)
        win->event_flags |= cmd::MousePress;

    if (pd->flags & cmd::MouseRelease)
        win->event_flags |= cmd::MouseRelease;

    if (pd->flags & cmd::MouseMove)
        win->event_flags |= cmd::MouseMove;

    if (pd->flags & cmd::KeyPress)
        win->event_flags |= cmd::KeyPress;

    if (pd->flags & cmd::KeyRelease)
        win->event_flags |= cmd::KeyRelease;

    r.error_code = 0;
    owner->send_response(r);
}


// ============================================================================
// Altera o nome da janela
// ============================================================================
void server::impl_window_title(session::ptr owner, cmd::window_title *pd)
{
    cmd::response r;
    r.packet_number = pd->packet_number;

    if (m_windows.find(pd->handle) == m_windows.end()) {
        r.error_code = 1;
        owner->send_response(r);
        return;
    }

    window::ptr win = m_windows[pd->handle];

    if (pd->length >= cmd::max_window_title)
        pd->length = cmd::max_window_title;

    pd->name[pd->length] = 0;
    win->title = std::string(pd->name);
    r.error_code = 0;
    owner->send_response(r);
}


// ============================================================================
// Desenha um ponto na janela
// ============================================================================
void server::impl_draw_point(session::ptr, cmd::draw_point *pd)
{
    // janela não existe
    if (m_windows.find(pd->handle) == m_windows.end()) {
        return;
    }

    ALLEGRO_COLOR col = al_map_rgba(pd->col.r, pd->col.g, pd->col.b, pd->col.a);

    window::ptr win = m_windows[pd->handle];
    push_draw_target();
    al_set_target_bitmap(win->surface);
    al_draw_pixel(pd->x, pd->y, col);
    pop_draw_target();
}


// ============================================================================
// Desenha uma linha na janela
// ============================================================================
void server::impl_draw_line(session::ptr, cmd::draw_line *pd)
{
    // janela não existe
    if (m_windows.find(pd->handle) == m_windows.end()) {
        return;
    }

    ALLEGRO_COLOR col = al_map_rgba(pd->col.r, pd->col.g, pd->col.b, pd->col.a);

    window::ptr win = m_windows[pd->handle];
    push_draw_target();
    al_set_target_bitmap(win->surface);
    al_draw_line(pd->x1, pd->y1, pd->x2, pd->y2, col, 1);
    pop_draw_target();
}


// ============================================================================
// Desenha um retângulo
// ============================================================================
void server::impl_draw_rect(session::ptr, cmd::draw_rect *pd)
{
    // janela não existe
    if (m_windows.find(pd->handle) == m_windows.end()) {
        return;
    }

    ALLEGRO_COLOR col = al_map_rgba(pd->col.r, pd->col.g, pd->col.b, pd->col.a);

    window::ptr win = m_windows[pd->handle];

    if (pd->r.w == -1)
        pd->r.w = win->rect.w - pd->r.x;
    if (pd->r.h == -1)
        pd->r.h = win->rect.h - pd->r.y;


    push_draw_target();
    al_set_target_bitmap(win->surface);

    al_draw_rectangle(pd->r.x, pd->r.y,
                      pd->r.x + pd->r.w, pd->r.y + pd->r.h,
                      col, 1);

    pop_draw_target();
}


// ============================================================================
// Desenha um retângulo preenchido
// ============================================================================
void server::impl_draw_filed_rect(session::ptr, cmd::draw_filled_rect *pd)
{
    // janela não existe
    if (m_windows.find(pd->handle) == m_windows.end()) {
        return;
    }

    ALLEGRO_COLOR col = al_map_rgba(pd->col.r, pd->col.g, pd->col.b, pd->col.a);

    window::ptr win = m_windows[pd->handle];

    if (pd->r.w == -1)
        pd->r.w = win->rect.w - pd->r.x;
    if (pd->r.h == -1)
        pd->r.h = win->rect.h - pd->r.y;

    push_draw_target();
    al_set_target_bitmap(win->surface);

    al_draw_filled_rectangle(pd->r.x, pd->r.y,
                             pd->r.x + pd->r.w, pd->r.y + pd->r.h,
                             col);

    pop_draw_target();
}


// ============================================================================
// Retorna informações sobre a janela
// ============================================================================
void server::impl_query_attributes(session::ptr owner, cmd::query_attributes *pd)
{
    // cria a resposta
    cmd::response r;
    r.packet_number = pd->packet_number;

    if (m_windows.find(pd->handle) == m_windows.end()) {
        r.error_code = 1;
        owner->send_response(r);
        return;
    }

    window::ptr win = m_windows[pd->handle];

    r.error_code = 0;
    cmd::resp_query_attributes& qa = r.attrb;
    qa.r = win->rect;
    owner->send_response(r);

}


// ============================================================================
// Define atributos da janela
// ============================================================================
void server::impl_set_attributes(session::ptr owner, cmd::set_attributes *pd)
{
    cmd::response r;
    r.packet_number = pd->packet_number;

    if (m_windows.find(pd->handle) == m_windows.end()) {
        r.error_code = 1;
        owner->send_response(r);
        return;
    }

    window::ptr win = m_windows[pd->handle];

    if (pd->flags & cmd::SetPosition) {
        win->rect.x = pd->rect.x;
        win->rect.y = pd->rect.y;
    }

    if (pd->flags & cmd::SetSize) {
        win->rect.w = pd->rect.w;
        win->rect.h = pd->rect.h;
    }

    r.error_code = 0;
    owner->send_response(r);
}


// ============================================================================
// Insere uma janela nas listas do servidor
// ============================================================================
void server::insert_window(window::ptr win)
{
    if (!win->parent)
        m_top_windows.push_back(win);
    m_windows[win->handle] = win;
}


// ============================================================================
// Remove uma janela das listas do servidor
// ============================================================================
void server::remove_window(window::ptr win)
{
    m_top_windows.remove(win);
    m_windows.erase(win->handle);
}


}
