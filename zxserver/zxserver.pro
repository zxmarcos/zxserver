TEMPLATE = app
CONFIG += console
CONFIG -= app_bundle
CONFIG -= qt
CONFIG += c++11 exceptions

SOURCES += main.cpp \
    zxserver.cpp \
    zxdisplay.cpp \
    zxdispatcher.cpp

HEADERS += \
    zxserver.h \
    zxpacket.h \
    zxcmd.h \
    zxbuffers.h \
    zxcolor.h \
    zxgeom.h

LIBS += -lboost_system -lallegro -lallegro_font -lallegro_ttf -lallegro_color -lallegro_primitives
