#pragma once
#include <cstdlib>
#include <memory>
#include "zxgeom.h"
#include "zxcolor.h"

namespace zx {

namespace cmd {

enum {
    max_window_title = 64
};

enum {
    ci_response = 0,

    // operações criadas pelo cliente
    ci_create_window,
    ci_destroy_window,
    ci_register_listener,
    ci_window_title,
    ci_query_attributes,
    ci_set_attributes,


    // operações de desenho (não tem resposta)
    ci_draw_point,
    ci_draw_line,
    ci_draw_rect,
    ci_draw_filled_rect,
    ci_draw_text,

    // respostas/eventos gerados pelo servidor
    ci_server_side_event = 0x80000000,
    ci_server_shutdown,
    ci_mouse_press_event,
    ci_mouse_release_event,
    ci_mouse_move_event,
    ci_keyboard_press_event,
    ci_keyboard_release_event,
    ci_grab_focus,
    ci_lost_focus,
};

// Sem padding nas estruturas que tem que ser idênticas ao serem transmitidas
#pragma pack(1)

// ============================================================================
// Classe base para comando e resposta do servidor
// ============================================================================
struct class_ {
    int packet_number;
    std::size_t size;
    int type;

    class_(std::size_t size=0, int type=0) :
        size(size), type(type) {
    }
    class_(const class_&) {
    }

    class_& operator=(const class_&) {
        return *this;
    }
    class_& operator=(const class_*) {
        return *this;
    }
};


// ============================================================================
// Respostas de eventos do lado do servidor
// ============================================================================
struct resp_mouse_event {
    int button;
    short x;
    short y;
    short z;
};

struct resp_keyboard_event {
    int keycode;
    int modifier;
    int unichar;
};

struct resp_focus {
    int handle;
};

struct resp_query_attributes {
    xrect r;
};


// ============================================================================
// Comando para criar uma nova janela
// ============================================================================
struct create_window : public class_ {
    short width;
    short height;
    short x;
    short y;
    int parent;
    char name[max_window_title];

    create_window() : class_(sizeof(create_window),
                             ci_create_window) {
    }
};

struct resp_create_window {
    int handle;
};

// ============================================================================
// Comando para destruir uma janela
// ============================================================================
struct destroy_window : public class_ {
    int handle;
    destroy_window() : class_(sizeof(destroy_window),
                              ci_destroy_window) {
    }
};

// ============================================================================
// Comando para pedir atributos da janela
// ============================================================================
struct query_attributes : public class_ {
    int handle;
    query_attributes() : class_(sizeof(query_attributes),
                                ci_query_attributes) {
    }
};

// ============================================================================
// Comando para setar atributos da janela
// ============================================================================
enum attributes_flag {
    SetSize = (1 << 0),
    SetPosition = (1 << 1),
};

struct set_attributes : public class_ {
    int handle;
    unsigned flags;
    xrect rect;
    set_attributes() : class_(sizeof(set_attributes),
                              ci_set_attributes) {
    }
};

// ============================================================================
// Pede ao servidor para enviar esses eventos para a janela
// ============================================================================
enum listener_flag {
    MousePress = 1,
    MouseRelease = 2,
    MouseMove = 4,
    KeyPress = 8,
    KeyRelease = 16
};

struct register_listener : public class_ {
    int handle;
    unsigned flags;
    register_listener() : class_(sizeof(register_listener),
                                 ci_register_listener) {
    }
};


// ============================================================================
// Define o título da janela
// ============================================================================
struct window_title : public class_ {
    int handle;
    int length;
    char name[max_window_title + 1];
    window_title() : class_(sizeof(window_title),
                            ci_window_title) {
        name[0] = 0;
        length = 0;
    }
    void from(const std::string& s) {
        auto size = s.size();
        if (size >= max_window_title)
            size = max_window_title;
        std::memcpy(name, s.data(), size);
        name[size] = '\0';
        length = size;
    }
};


// ============================================================================
// Desenha um ponto na janela
// ============================================================================
struct draw_point : public class_ {
    int handle;
    int x;
    int y;
    xcolor col;
    draw_point() : class_(sizeof(draw_point),
                          ci_draw_point) {
    }
};


// ============================================================================
// Desenha uma linha na janela
// ============================================================================
struct draw_line : public class_ {
    int handle;
    int x1;
    int y1;
    int x2;
    int y2;
    xcolor col;
    draw_line() : class_(sizeof(draw_line),
                         ci_draw_line) {
    }
};


// ============================================================================
// Desenha um retângulo
// ============================================================================
struct draw_rect : public class_ {
    int handle;
    xrect r;
    xcolor col;
    draw_rect() : class_(sizeof(draw_rect),
                         ci_draw_rect) {
    }
};


// ============================================================================
// Desenha um retângulo preenchido
// ============================================================================
struct draw_filled_rect : public class_ {
    int handle;
    xrect r;
    xcolor col;
    draw_filled_rect() : class_(sizeof(draw_filled_rect),
                         ci_draw_filled_rect) {
    }
};

// ============================================================================
// Descrição de uma resposta do servidor
// ============================================================================
struct response : public class_ {
    typedef std::shared_ptr<response> ptr;
    short error_code;
    union {
        resp_create_window create_win;
        resp_mouse_event mouse;
        resp_keyboard_event keyboard;
        resp_focus focus;
        resp_query_attributes attrb;
    };

    // precisamos de todos esses métodos de cópia...
    response& operator=(const response& rhs) {
        std::memcpy(static_cast<class_*>(this), &rhs, sizeof(response));
        return *this;
    }

    response& operator=(const response* rhs) {
        std::memcpy(static_cast<class_*>(this), rhs, sizeof(response));
        return *this;
    }
    response(const response& rhs) : class_() {
        std::memcpy(static_cast<class_*>(this), &rhs, sizeof(response));
    }

    response(int error_code=0) : class_(sizeof(response), ci_response),
        error_code(error_code)
    {
        packet_number = 0;
    }
};


}

}
