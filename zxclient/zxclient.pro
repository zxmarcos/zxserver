TEMPLATE = app
CONFIG += console
CONFIG -= app_bundle
CONFIG -= qt
CONFIG += c++11
SOURCES += main.cpp \
    zxlib.cpp

CONFIG += exceptions
LIBS += -lboost_system

HEADERS += \
    zxlib.h

LIBS += -pthread
INCLUDEPATH += ../zxserver

QMAKE_CXXFLAGS += -Wno-unused-parameter
