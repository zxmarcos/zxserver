
#include <iostream>
#include <cstring>
#include <boost/asio.hpp>
#include <atomic>
#include <thread>
#include <map>
#include <mutex>
#include <exception>
#include <condition_variable>
#include "zxcmd.h"
#include "zxpacket.h"
#include "zxlib.h"
#include "zxbuffers.h"

namespace zx
{

// ============================================================================
// Cria a sessão com o servidor
// ============================================================================
session::session(asio::io_service &service, int port) :
    m_service(service), m_socket(service)
{
    m_server = tcp::endpoint(tcp::v4(), port);
    try {
        m_socket.connect(m_server);
    } catch (std::exception& e) {
        std::cerr << "Não foi possível conectar ao servidor!" << std::endl;
        std::cerr << e.what() << std::endl;
    }
}


// ============================================================================
// Inicia a sessão com o servidor
// ============================================================================
void session::start()
{
    std::thread t([&] { do_read_header(); m_service.run(); });
    t.detach();
}


// ============================================================================
// Cria uma janela
// ============================================================================
int session::create_window(int x, int y, int w, int h, int parent)
{
    cmd::create_window cw;
    cw.packet_number = generate_packet_number();
    cw.height = h;
    cw.width = w;
    cw.x = x;
    cw.y = y;
    cw.parent = parent;
    send_command(&cw, true);

    cmd::response::ptr r(wait_for_response(cw.packet_number));
    if (r->error_code != 0) {
        std::cerr << "Falha ao criar a janela!" << std::endl;
        return 0;   
    }

    std::cout << "janela criada: handle " << r->create_win.handle << std::endl;
    return r->create_win.handle;
}


// ============================================================================
// Registra a janela para receber eventos
// ============================================================================
int session::register_listener(int handle, unsigned flags)
{
    cmd::register_listener d;
    d.packet_number = generate_packet_number();
    d.flags = flags;
    d.handle = handle;
    send_command(&d, true);

    cmd::response::ptr r(wait_for_response(d.packet_number));
    if (r->error_code != 0) {
        std::cerr << "Falha ao registrar a janela nos listeners!" << std::endl;
        return 0;
    }

    std::cout << "listener registrado" << std::endl;
    return 1;
}


// ============================================================================
// Altera o nome da janela
// ============================================================================
int session::set_window_title(int handle, std::string title)
{
    cmd::window_title d;
    d.packet_number = generate_packet_number();
    d.handle = handle;
    d.from(title);
    send_command(&d, true);

    cmd::response::ptr r(wait_for_response(d.packet_number));
    if (r->error_code != 0) {
        std::cerr << "Falha ao alterar o nome da janela!" << std::endl;
        return 0;
    }

    std::cout << "Nome alterado com suceso!" << std::endl;
    return 1;
}

void session::set_pos(int handle, int x, int y)
{
    cmd::set_attributes d;
    d.packet_number = generate_packet_number();
    d.handle = handle;
    d.flags = cmd::SetPosition;
    d.rect.x = x;
    d.rect.y = y;
    send_command(&d, true);

    cmd::response::ptr r(wait_for_response(d.packet_number));
    if (r->error_code != 0) {
        std::cerr << "Falha ao alterar a posição da janela!" << std::endl;
    }
}

void session::set_size(int handle, int w, int h)
{
    cmd::set_attributes d;
    d.packet_number = generate_packet_number();
    d.handle = handle;
    d.flags = cmd::SetSize;
    d.rect.w = w;
    d.rect.h = h;
    send_command(&d, true);

    cmd::response::ptr r(wait_for_response(d.packet_number));
    if (r->error_code != 0) {
        std::cerr << "Falha ao alterar o tamanho da janela!" << std::endl;
    }
}


// ============================================================================
// Desenha uma linha
// ============================================================================
void session::draw_line(int handle, int x1, int y1, int x2, int y2, xcolor col)
{
    cmd::draw_line d;
    d.packet_number = generate_packet_number();
    d.handle = handle;
    d.x1 = x1;
    d.y1 = y1;
    d.x2 = x2;
    d.y2 = y2;
    d.col = col;
    send_command(&d);
}

void session::draw_rect(int handle, const xrect& rect, const xcolor& col)
{
    cmd::draw_rect d;
    d.packet_number = generate_packet_number();
    d.handle = handle;
    d.r = rect;
    d.col = col;
    send_command(&d);
}

void session::draw_filled_rect(int handle, const xrect& rect, const xcolor& col)
{
    cmd::draw_filled_rect d;
    d.packet_number = generate_packet_number();
    d.handle = handle;
    d.r = rect;
    d.col = col;
    send_command(&d);
}


// ============================================================================
// Limpa toda a janela
// ============================================================================
void session::clear(int handle, xcolor col)
{
    cmd::draw_filled_rect d;
    d.packet_number = generate_packet_number();
    d.handle = handle;
    d.r.x = 0;
    d.r.y = 0;
    d.r.w = -1;
    d.r.h = -1;
    d.col = col;
    send_command(&d);
}


// ============================================================================
// Espera pela resposta de um pacote
// ============================================================================
cmd::response::ptr session::wait_for_response(int packet, int timeout)
{
    // Esse pacote está marcado como sem resposta!
    if (m_arrived.find(packet) == m_arrived.end()) {
        return std::make_shared<cmd::response>();
    }

    while (!m_arrived[packet]) {
        std::this_thread::sleep_for(std::chrono::milliseconds(1));
    }

    cmd::response::ptr resp = m_responses[packet];
    m_arrived.erase(packet);
    m_responses.erase(packet);
    return resp;
}


// ============================================================================
// Envia um comando
// ============================================================================
void session::send_command(cmd::class_* base, bool set_on_arrive)
{
    cmd_packet packet;

    packet.body_length(base->size);
    packet.encode_header();
    assert(base->size < cmd_packet::max_body_length);

    std::memcpy(packet.body(), base, base->size);

    auto buf = shared_const_buffer(packet.data(), packet.length());

    if (set_on_arrive)
        m_arrived[base->packet_number] = false;
    else
        m_arrived.erase(base->packet_number);

    asio::async_write(m_socket, buf, [&](error_code, std::size_t){});
}


// ============================================================================
// Manipula e despacha as respostas e eventos que o servidor nos enviou
// ============================================================================
void session::handle_received_packet()
{
    cmd::class_* p = reinterpret_cast<cmd::class_*>(m_read_cmd.body());

    bool is_event = (p->type & cmd::ci_server_side_event) == cmd::ci_server_side_event;
    // é uma resposta
    if (p->type == cmd::ci_response && !is_event) {
        std::cout << "Recebeu resposta do pacote: " << p->packet_number << std::endl;
        cmd::response* rp = static_cast<cmd::response*>(p);
        m_responses[rp->packet_number] = std::make_shared<cmd::response>(*rp);;

        // diz que recebeu o pacote
        m_arrived[rp->packet_number] = true;

    } else {
        cmd::response* rp = static_cast<cmd::response*>(p);
        switch (p->type) {
        case cmd::ci_mouse_press_event:
            std::cout << "[mouse_press] " << rp->mouse.button << std::endl;
            break;
        case cmd::ci_mouse_release_event:
            std::cout << "[mouse_release] " << rp->mouse.button << std::endl;
            break;
        case cmd::ci_mouse_move_event:
            std::cout << "[mouse_move] " << rp->mouse.x << ',' << rp->mouse.y << ',' << rp->mouse.z << std::endl;
            break;
        case cmd::ci_keyboard_press_event:
            std::cout << "[key_press] " << rp->keyboard.keycode << ',' << rp->keyboard.unichar << std::endl;
            break;
        case cmd::ci_keyboard_release_event:
            std::cout << "[key_release] " << rp->keyboard.keycode << ',' << rp->keyboard.unichar << std::endl;
            break;
        case cmd::ci_server_shutdown:
            std::cout << "[server_shutdown]" << std::endl;
            std::exit(0);
            break;
        case cmd::ci_lost_focus:
            std::cout << "[lost_focus] " << rp->focus.handle << std::endl;
            break;
        case cmd::ci_grab_focus:
            std::cout << "[grab_focus] " << rp->focus.handle << std::endl;
            break;
        }
    }
}


// ============================================================================
// Lê o cabeçalho de uma resposta do servidor
// ============================================================================
void session::do_read_header()
{
    auto self(shared_from_this());
    auto buffer = asio::buffer(m_read_cmd.data(), cmd_packet::header_length);

    auto reader = [this, self](error_code ec, std::size_t) {
        if (!ec && m_read_cmd.decode_header())
            do_read_body();
    };

    // faz a leitura assíncrona do cabeçalho
    asio::async_read(m_socket, buffer, reader);
}


// ============================================================================
// Lê o corpo de um comando que o servidor nos enviou
// ============================================================================
void session::do_read_body()
{
    auto self(shared_from_this());
    auto buffer = asio::buffer(m_read_cmd.body(), m_read_cmd.body_length());

    auto reader = [this, self](error_code ec, std::size_t) {
        if (!ec) {
            handle_received_packet();
            do_read_header();
        }
    };

    // faz a leitura assíncrona do corpo do comando
    asio::async_read(m_socket, buffer, reader);
}


// ============================================================================
// Gera um número de identificação para um pacote.
// Cada pacote enviado tem um número único de identificação
// ============================================================================
int session::generate_packet_number()
{
    static std::atomic_int counter(5000);
    return counter++;
}


session::ptr session::ms_default_instance;

void session::set_default_session(session::ptr p)
{
    ms_default_instance = p;
}

session::ptr session::get_instance()
{
    return ms_default_instance;
}

}
