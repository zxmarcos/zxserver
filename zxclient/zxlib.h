
#pragma once

#include <iostream>
#include <cstring>
#include <boost/asio.hpp>
#include <atomic>
#include <thread>
#include <map>
#include <mutex>
#include <condition_variable>
#include "zxcmd.h"
#include "zxpacket.h"
#include "zxcolor.h"
#include "zxgeom.h"

namespace zx
{

namespace asio = boost::asio;
using asio::ip::tcp;
using boost::system::error_code;

class session;


// Classe responsável por gerenciar a conexão com o servidor
class session : public std::enable_shared_from_this<session> {
    asio::io_service& m_service;
    tcp::endpoint m_server;
    tcp::socket m_socket;

    // um pacote lido por vez...
    cmd_packet m_read_cmd;

    // Espera pela resposta de um pacote
    cmd::response::ptr wait_for_response(int packet, int timeout=1000);
    // Envia uma comando
    void send_command(cmd::class_* base, bool set_on_arrive=false);
    void handle_received_packet();
    void do_read_header();
    void do_read_body();
    int generate_packet_number();

    std::map<int, bool> m_arrived;
    std::map<int, cmd::response::ptr> m_responses;

    static std::shared_ptr<session> ms_default_instance;
public:
    typedef std::shared_ptr<session> ptr;
    session(asio::io_service& service, int port);
    static void set_default_session(ptr p);
    static ptr get_instance();
    void start();
    void clear(int handle, xcolor col);
    int create_window(int x, int y, int w, int h, int parent=0);
    void draw_line(int handle, int x1, int y1, int x2, int y2, xcolor col);
    void draw_rect(int handle, const xrect& rect, const xcolor& col);
    void draw_filled_rect(int handle, const xrect& rect, const xcolor& col);

    int register_listener(int handle, unsigned flags);
    int set_window_title(int handle, std::string title);
    void set_pos(int handle, int x, int y);
    void set_size(int handle, int w, int h);

};

};

