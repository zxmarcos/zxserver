#include <cstdlib>
#include <iostream>
#include <thread>
#include <utility>
#include <string>
#include <boost/array.hpp>
#include <boost/asio.hpp>
#include "zxlib.h"
#include "zxcmd.h"
#include <cstdlib>
#include <ctime>

using namespace zx;

int main(int, char**)
{
    asio::io_service service;

    try
    {
        session::ptr s = session::ptr(new session(service, 7070));
        s->start();
        session::set_default_session(s);

        for (;;) {
            std::this_thread::sleep_for(std::chrono::milliseconds(100));
        }
    }
    catch (std::exception& e)
    {
        std::cerr << "Exception: " << e.what() << "\n";
    }
    return 0;
}
